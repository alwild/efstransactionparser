﻿using System;
using FileHelpers;

namespace EFSTransactionParser
{
    [FixedLengthRecord]
    public class EFSTransactionLocation: IEFSRecord
    {
        [FieldFixedLength(2)]
        [FieldTrim(TrimMode.Both)]
        public string RecordType;

        [FieldFixedLength(2)]
        [FieldTrim(TrimMode.Both)]
        public string Version;

        [FieldFixedLength(10)]
        [FieldTrim(TrimMode.Both)]
        public string Sequence;

        [FieldFixedLength(10)]
        [FieldAlign(AlignMode.Right, '0')]
        public int LocationId;

        [FieldFixedLength(4)]
        [FieldTrim(TrimMode.Both)]
        public string LocationTypeCode;

        [FieldFixedLength(25)]
        [FieldTrim(TrimMode.Both)]
        public string LocationName;

        [FieldFixedLength(25)]
        [FieldTrim(TrimMode.Both)]
        public string LocationCity;

        [FieldFixedLength(2)]
        [FieldTrim(TrimMode.Both)]
        public string LocationState;

        [FieldFixedLength(1)]
        public string LocationCountry;

        [FieldFixedLength(11)]
        [FieldTrim(TrimMode.Both)]
        public string OpisID;

        [FieldFixedLength(1)]
        public string POSTimeZone;

        [FieldFixedLength(3)]
        public string ChainCode;

        [FieldFixedLength(10)]
        [FieldAlign(AlignMode.Right, '0')]
        [FieldNullValue(0)]
        public int TransactionId;

        [FieldFixedLength(7)]
        [FieldAlign(AlignMode.Right, '0')]
        [FieldNullValue(0)]
        public int PumpNumber;

        [FieldFixedLength(187)]
        [FieldTrim(TrimMode.Both)]
        public string Filler;

        public override string GetFiller()
        {
            return Filler;
        }

        public static string GetRecordType()
        {
            return "LL";
        }
    }

    [DelimitedRecord(",")]
    public class TransactionLocation
    {
        public string RecordType;
        public string Version;
        public string Sequence;
        public int LocationId;
        public string LocationTypeCode;
        public string LocationName;
        public string LocationCity;
        public string LocationState;
        public string LocationCountry;
        public string OpisID;
        public string POSTimeZone;
        public string ChainCode;
        public int TransactionId;
        public int PumpNumber;
        public string Filler;
        public string FileName;
    }
}
