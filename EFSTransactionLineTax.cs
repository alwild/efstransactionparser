﻿using System;
using FileHelpers;

namespace EFSTransactionParser
{
    [FixedLengthRecord]
    public class EFSTransactionLineTax: IEFSRecord
    {
        [FieldFixedLength(2)]
        [FieldTrim(TrimMode.Both)]
        public string RecordType;

        [FieldFixedLength(2)]
        [FieldTrim(TrimMode.Both)]
        public string Version;

        [FieldFixedLength(10)]
        [FieldTrim(TrimMode.Both)]
        public string SequenceNumber;

        [FieldFixedLength(10)]
        [FieldAlign(AlignMode.Right, '0')]
        public int TransactionId;

        [FieldFixedLength(2)]
        [FieldAlign(AlignMode.Right, '0')]
        public int LineId;

        [FieldFixedLength(10)]
        [FieldTrim(TrimMode.Both)]
        public string TaxDescription;

        [FieldFixedLength(10)]
        [FieldTrim(TrimMode.Both)]
        public string TaxCode;

        [FieldFixedLength(11)]
        [FieldAlign(AlignMode.Right, '0')]
        [FieldConverter(typeof(DecimalConverter))]
        public double TaxAmount;

        [FieldFixedLength(1)]
        public string GrossNetFlag;

        [FieldFixedLength(1)]
        public string ExemptFlag;

        [FieldFixedLength(1)]
        public string TaxRateType;

        [FieldFixedLength(11)]
        [FieldAlign(AlignMode.Right, '0')]
        [FieldConverter(typeof(DecimalConverter), 4)]
        public double TaxRate;

        [FieldFixedLength(229)]
        [FieldTrim(TrimMode.Both)]
        public string Filler;

        public override string GetFiller()
        {
            return Filler;
        }

        public static string GetRecordType()
        {
            return "TX";
        }
    }

    [DelimitedRecord(",")]
    public class TransactionLineTax
    {
        public string RecordType;
        public string Version;
        public string SequenceNumber;
        public int TransactionId;
        public int LineId;
        public string TaxDescription;
        public string TaxCode;
        public double TaxAmount;
        public string GrossNetFlag;
        public string ExemptFlag;
        public string TaxRateType;
        public double TaxRate;
        public string Filler;
        public string FileName;
    }
}
