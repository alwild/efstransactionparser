﻿using System;
using FileHelpers;

namespace EFSTransactionParser
{
    [FixedLengthRecord]
    public class EFSTransactionItem: IEFSRecord
    {
        [FieldFixedLength(2)]
        [FieldTrim(TrimMode.Both)]
        public string RecordType;

        [FieldFixedLength(2)]
        [FieldTrim(TrimMode.Both)]
        public string Version;

        [FieldFixedLength(10)]
        [FieldTrim(TrimMode.Both)]
        public string Sequence;

        [FieldFixedLength(10)]
        [FieldAlign(AlignMode.Right, '0')]
        public int TransactionId;

        [FieldFixedLength(4)]
        [FieldTrim(TrimMode.Both)]
        public string InfoIdCode;

        [FieldFixedLength(24)]
        [FieldTrim(TrimMode.Both)]
        public string InfoValue;

        [FieldFixedLength(248)]
        [FieldTrim(TrimMode.Both)]
        public string Filler;

        public override string GetFiller()
        {
            return Filler;
        }

        public static string GetRecordType()
        {
            return "TI";
        }
    }

    [DelimitedRecord(",")]
    public class TransactionItem
    {
        public string RecordType;
        public string Version;
        public string Sequence;
        public int TransactionId;
        public string InfoIdCode;
        public string InfoValue;
        public string Filler;
        public string FileName;
    }
}
