﻿using System;
using FileHelpers;
using PetaPoco;

namespace EFSTransactionParser
{
    [FixedLengthRecord]
    public class EFSTransactionSummary : IEFSRecord
    {
        [FieldFixedLength(2)]
        [FieldTrim(TrimMode.Both)]
        public string RecordType;

        [FieldFixedLength(2)]
        [FieldTrim(TrimMode.Both)]
        public string Version;

        [FieldFixedLength(10)]
        [FieldTrim(TrimMode.Both)]
        public string SequenceNumber;

        [FieldFixedLength(10)]
        [FieldAlign(AlignMode.Right, '0')]
        public int TransactionId;

        [FieldFixedLength(25)]
        [FieldTrim(TrimMode.Both)]
        public string CardNumber;

        [FieldFixedLength(12)]
        [FieldTrim(TrimMode.Both)]
        public string InvoiceNumber;

        [FieldFixedLength(10)]
        [FieldAlign(AlignMode.Right, '0')]
        public int LocationId;

        [FieldFixedLength(12)]
        [FieldConverter(ConverterKind.Date, "yyyyMMddHHmm")]
        public DateTime TransactionDate;

        [FieldFixedLength(10)]
        [FieldTrim(TrimMode.Both)]
        public string AuthorizationCode;

        [FieldFixedLength(10)]
        [FieldAlign(AlignMode.Right, '0')]
        public int ContractId;

        [FieldFixedLength(10)]
        [FieldAlign(AlignMode.Right, '0')]
        public int IssuerId;

        [FieldFixedLength(10)]
        [FieldAlign(AlignMode.Right, '0')]
        public int CarrierId;

        [FieldFixedLength(10)]
        [FieldAlign(AlignMode.Right, '0')]
        public int SupplierId;

        [FieldFixedLength(14)]
        [FieldTrim(TrimMode.Both)]
        public string ReportedCarrier;

        [FieldFixedLength(1)]
        public string HandEntered;

        [FieldFixedLength(1)]
        public string OverrideCard;

        [FieldFixedLength(11)]
        [FieldConverter(typeof(DecimalConverter))]
        [FieldAlign(AlignMode.Right, '0')]
        public double TotalDiscountAmount;

        [FieldFixedLength(11)]
        [FieldConverter(typeof(DecimalConverter))]
        [FieldAlign(AlignMode.Right, '0')]
        public double CarrierFee;

        [FieldFixedLength(11)]
        [FieldConverter(typeof(DecimalConverter))]
        [FieldAlign(AlignMode.Right, '0')]
        public double SupplierFee;

        [FieldFixedLength(11)]
        [FieldTrim(TrimMode.Both)]
        public string SettlementId;

        [FieldFixedLength(11)]
        [FieldTrim(TrimMode.Both)]
        public string StatementId;

        [FieldFixedLength(1)]
        public string DiscountType;

        [FieldFixedLength(14)]
        [FieldConverter(ConverterKind.Date, "yyyyMMddHHmmss")]
        public DateTime POSDate;

        [FieldFixedLength(3)]
        [FieldTrim(TrimMode.Both)]
        public string CurrencyCode;

        [FieldFixedLength(11)]
        [FieldAlign(AlignMode.Right, '0')]
        [FieldConverter(typeof(DecimalConverter), 4)]
        public double ConversionRate;

        [FieldFixedLength(11)]
        [FieldAlign(AlignMode.Right, '0')]
        [FieldConverter(typeof(DecimalConverter))]
        public double PreferTotal;

        [FieldFixedLength(1)]
        public string Country;

        [FieldFixedLength(1)]
        public string BillingCountry;

        [FieldFixedLength(11)]
        [FieldAlign(AlignMode.Right, '0')]
        [FieldConverter(typeof(DecimalConverter))]
        public double InvoiceTotal;

        [FieldFixedLength(1)]
        public string TransactionType;

        [FieldFixedLength(1)]
        public string IsCredit;

        [FieldFixedLength(3)]
        public string TransactionSystemType;

        [FieldFixedLength(11)]
        [FieldTrim(TrimMode.Both)]
        public string Filler;

        [FieldFixedLength(4)]
        [FieldAlign(AlignMode.Right, '0')]
        public int CardPolicy;

        [FieldFixedLength(11)]
        [FieldAlign(AlignMode.Right, '0')]
        [FieldConverter(typeof(DecimalConverter))]
        public double NetTotalDollars;

        [FieldFixedLength(11)]
        [FieldAlign(AlignMode.Right, '0')]
        [FieldConverter(typeof(DecimalConverter))]
        public double FundedTotal;

        [FieldFixedLength(1)]
        public string ExcludeFlag;

        /*
        [FieldFixedLength(41)]
        [FieldTrim(TrimMode.Both)]
        [FieldNullValue("")]
        public string Filler;
        */
        public override string GetFiller()
        {
            return Filler;
        }

        public static string GetRecordType()
        {
            return "TA";
        }
    }

    [DelimitedRecord(",")]
    public class TransactionSummary
    {
        public string RecordType;
        public string Version;
        public string SequenceNumber;
        public int TransactionId;
        public string CardNumber;
        public string InvoiceNumber;
        public int LocationId;
        public DateTime TransactionDate;
        public string AuthorizationCode;
        public int ContractId;
        public int IssuerId;
        public int CarrierId;
        public int SupplierId;
        public string ReportedCarrier;
        public string HandEntered;
        public string OverrideCard;
        public double TotalDiscountAmount;
        public double CarrierFee;
        public double SupplierFee;
        public string SettlementId;
        public string StatementId;
        public string DiscountType;
        public DateTime POSDate;
        public string CurrencyCode;
        public double ConversionRate;
        public double PreferTotal;
        public string Country;
        public string BillingCountry;
        public double InvoiceTotal;
        public string TransactionType;
        public string IsCredit;
        public string TransactionSystemType;
        public int CardPolicy;
        public double NetTotalDollars;
        public double FundedTotal;
        public string ExcludeFlag;
        public string Filler;
        public string FileName;
    }
}


