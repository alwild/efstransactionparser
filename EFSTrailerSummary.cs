﻿using System;
using FileHelpers;

namespace EFSTransactionParser
{
    [FixedLengthRecord]
    public class EFSTrailerSummary: IEFSRecord
    {
        [FieldFixedLength(2)]
        [FieldTrim(TrimMode.Both)]
        public string RecordType;

        [FieldFixedLength(2)]
        [FieldTrim(TrimMode.Both)]
        public string Version;

        [FieldFixedLength(10)]
        [FieldTrim(TrimMode.Both)]
        public string Sequence;

        [FieldFixedLength(10)]
        [FieldAlign(AlignMode.Right, '0')]
        public int TotalNumberTransactions;

        [FieldFixedLength(10)]
        [FieldAlign(AlignMode.Right, '0')]
        public int TotalNumberRecords;

        [FieldFixedLength(1)]
        public string TotalTransactionSign;

        [FieldFixedLength(11)]
        [FieldAlign(AlignMode.Right, '0')]
        [FieldConverter(typeof(DecimalConverter))]
        public double TotalTransactionAmount;

        [FieldFixedLength(254)]
        [FieldTrim(TrimMode.Both)]
        public string Filler;

        public override string GetFiller()
        {
            return Filler;
        }

        public static string GetRecordType()
        {
            return "HT";
        }
    }

    [DelimitedRecord(",")]
    public class TrailerSummary
    {
        public string RecordType;
        public string Version;
        public string Sequence;
        public int TotalNumberTransactions;
        public int TotalNumberRecords;
        public string TotalTransactionSign;
        public double TotalTransactionAmount;
        public string Filler;
        public string FileName;
    }
}
