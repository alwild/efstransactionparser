﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace EFSTransactionParser
{
    public abstract class IEFSRecord
    {
        public abstract string GetFiller();
    }
}
