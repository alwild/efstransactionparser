﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShipExDB
{

    public interface ISavable
    {
        void Save();
        DateTime CreatedDate { get; set; }
        string FileName { get; set; }
    }

    public interface IExists
    {
        bool Exists();
    }

    public partial class TransactionSummary : ISavable { }
    public partial class TransactionLine : ISavable { }
    public partial class TransactionItem : ISavable { }
    public partial class TransactionLineTax : ISavable { }
    public partial class TransactionLocation : ISavable
    {
        public bool Exists()
        {
            return TransactionSummary.Exists("WHERE LocationId=@0", LocationId);
        }
    }
}
