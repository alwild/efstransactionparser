DROP TABLE [dbo].[TransactionSummary]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TransactionSummary](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RecordType] [varchar](10) NOT NULL,
	[Version] [varchar](10) NOT NULL,
	[SequenceNumber] [varchar](50) NOT NULL,
	[TransactionId] [int] NOT NULL,
	[CardNumber] [varchar](25) NULL,
	[InvoiceNumber] [varchar](50) NULL,
	[LocationId] [int] NOT NULL,
	[TransactionDate] [datetime] NOT NULL,
	[AuthorizationCode] [varchar](50) NULL,
	[ContractId] [int] NOT NULL,
	[IssuerId] [int] NOT NULL,
	[CarrierId] [int] NOT NULL,
	[SupplierId] [int] NOT NULL,
	[ReportedCarrier] [varchar](50) NULL,
	[HandEntered] [varchar](50) NULL,
	[OverrideCard] [varchar](50) NULL,
	[TotalDiscountAmount] [decimal](18, 2) NOT NULL,
	[CarrierFee] [decimal](18, 2) NOT NULL,
	[SupplierFee] [decimal](18, 2) NOT NULL,
	[SettlementId] [varchar](50) NULL,
	[StatementId] [varchar](50) NULL,
	[DiscountType] [varchar](50) NULL,
	[POSDate] [datetime] NOT NULL,
	[CurrencyCode] [varchar](50) NULL,
	[ConversionRate] [decimal](18, 4) NOT NULL,
	[PreferTotal] [decimal](18, 2) NOT NULL,
	[Country] [varchar](50) NULL,
	[BillingCountry] [varchar](50) NULL,
	[InvoiceTotal] [decimal](18, 2) NOT NULL,
	[TransactionType] [varchar](50) NULL,
	[IsCredit] [varchar](50) NULL,
	[Filler] [varchar](50) NULL,
	[CreatedDate] [datetime] NOT NULL default(GetDate()),
	[FileName] varchar(200)
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

DROP TABLE [dbo].[TransactionLocation]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TransactionLocation]
(
	[ID] int not null identity(1,1),
	[RecordType] [varchar](10) NULL,
	[Version] [varchar](10) NULL,
	[Sequence] [varchar](10) NULL,
	[LocationId] [int] NOT NULL,
	[LocationTypeCode] [varchar](50) NULL,
	[LocationName] [varchar](50) NULL,
	[LocationCity] [varchar](50) NULL,
	[LocationState] [varchar](50) NULL,
	[LocationCountry] [varchar](50) NULL,
	[OpisID] [varchar](50) NULL,
	[Filler] [varchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL Default(GetDate()),
	[FileName] varchar(255)
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE UNIQUE NONCLUSTERED INDEX [IDX_TransactionLocation_LocationId] ON [dbo].[TransactionLocation]
(
	[LocationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



SET ANSI_PADDING OFF
GO


DROP TABLE [dbo].[TransactionLineTax]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TransactionLineTax](
	ID int not null identity(1,1) primary key,
	[RecordType] [varchar](10) NULL,
	[Version] [varchar](10) NULL,
	[SequenceNumber] [varchar](10) NULL,
	[TransactionId] int not null,
	[LineId] int not null,
	[TaxDescription] [varchar](50) NULL,
	[TaxCode] [varchar](50) NULL,
	[TaxAmount] decimal(18,2) not null,
	[GrossNetFlag] [varchar](50) NULL,
	[ExemptFlag] [varchar](50) NULL,
	[TaxRateType] [varchar](50) NULL,
	[TaxRate] decimal(18,2) not null,
	[Filler] [varchar](500) NULL,
	CreatedDate datetime not null default(GetDate()),
	[FileName] varchar(255)
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


DROP TABLE [dbo].[TransactionLine]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TransactionLine](
	[ID] int not null identity(1,1) primary key,
	[RecordType] [varchar](10) NULL,
	[Version] [varchar](10) NULL,
	[SequenceNumber] [varchar](10) NULL,
	[TransactionId] int not null,
	[LineId] int not null,
	[ProductCategory] [varchar](50) NULL,
	[Quantity] decimal(18,2) not null,
	[AmountDiscounted] decimal(18,2) not null,
	[RetailAmount] decimal(18,2) not null,
	[FuelType] int not null,
	[ServiceType] [varchar](50) NULL,
	[FuelUseType] [varchar](50) NULL,
	[PPUDiscounted] decimal(18,3) not null,
	[RetailPPU] decimal(18,3) not null,
	[ProductNumber] [varchar](50) NULL,
	[UnitOfMeasure] [varchar](50) NULL,
	[DiscountAmount] decimal(18,2) not null,
	[Filler] [varchar](50) NULL,
	[CreatedDate] datetime not null default(GetDate()),
	[FileName] varchar(255)
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


DROP TABLE [dbo].[TransactionItem]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TransactionItem](
	[ID] int not null identity(1,1) primary key,
	[RecordType] [varchar](10) NULL,
	[Version] [varchar](10) NULL,
	[Sequence] [varchar](10) NULL,
	[TransactionId] int not null,
	[InfoIdCode] [varchar](500) NULL,
	[InfoValue] [varchar](500) NULL,
	[Filler] [varchar](500) NULL,
	CreatedDate datetime not null default(GetDate()),
	[FileName] varchar(255)
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


