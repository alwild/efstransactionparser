﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;
using log4net;
using System.IO;
using System.Configuration;
using Limilabs.FTP;

namespace EFSTransactionParser
{
    class Program
    {
        private static Type[] recordTypes = { typeof(EFSFileHeader), typeof(EFSTransactionSummary), typeof(EFSTrailerSummary),
            typeof(EFSTransactionItem), typeof(EFSTransactionLine), typeof(EFSTransactionLineTax), typeof(EFSTransactionLocation),
            typeof(EFSTransactionMisc), typeof(EFSTransactionTrailer)};

        private static Type[] outputTypes = { typeof(FileHeader), typeof(TransactionSummary), typeof(TrailerSummary),
            typeof(TransactionItem), typeof(TransactionLine), typeof(TransactionLineTax), typeof(TransactionLocation),
            typeof(TransactionMisc), typeof(TransactionTrailer)};

        public static Type[] dbTypes = { null, typeof(ShipExDB.TransactionSummary), null,
            typeof(ShipExDB.TransactionItem), typeof(ShipExDB.TransactionLine), typeof(ShipExDB.TransactionLineTax), typeof(ShipExDB.TransactionLocation),
            null, null };

        public static string DataFolder = "";
        public static string ProcessedFolder = ""; 

        public static Type EFSRecordTypeSelector(MultiRecordEngine engine, string recordString)
        {
            logger.Debug(recordString);
            foreach(var item in recordTypes)
            {
                var recordType = item.GetMethod("GetRecordType").Invoke(null, null);
                if (recordString.StartsWith(recordType.ToString())) return item;
            }
            throw new Exception("RecordTypeSelector Not Found");
        }

        public static log4net.ILog logger = log4net.LogManager.GetLogger("DefaultLogger");

        public static void Main(string[] args)
        {
            try
            {
                log4net.Config.XmlConfigurator.Configure();
                DataFolder = ConfigurationManager.AppSettings["DOWNLOAD_FOLDER"];
                ProcessedFolder = ConfigurationManager.AppSettings["PROCESSED_FOLDER"];
                if (!Directory.Exists(DataFolder))
                {
                    Directory.CreateDirectory(DataFolder);
                }

                if (!Directory.Exists(ProcessedFolder))
                {
                    Directory.CreateDirectory(ProcessedFolder);
                }

                //download files
                var downloadedFiles = DownloadFiles();

                //process files
                foreach(var filename in downloadedFiles)
                {
                    ProcessFile(filename);
                    FTPMove(filename);
                    File.Move(filename, Path.Combine(ProcessedFolder, Path.GetFileName(filename)));
                }
            }
            catch(Exception ex)
            {
                logger.Fatal(ex);
                throw;
            }
        }

        public static string[] DownloadFiles()
        {
            logger.Info("Downloading Files");
            using (var client = new Limilabs.FTP.Client.Ftp())
            {
                client.Connect(ConfigurationManager.AppSettings["FTP_HOST"]);
                client.Login(ConfigurationManager.AppSettings["FTP_USERNAME"], ConfigurationManager.AppSettings["FTP_PASSWORD"]);
                var options = new Limilabs.FTP.Client.RemoteSearchOptions("*.csv", false);
                client.DownloadFiles("/", DataFolder, options);
                return Directory.GetFiles(DataFolder);
            }
        }

        public static void FTPMove(string filename)
        {
            var fname = Path.GetFileName(filename);
            var bytes = File.ReadAllBytes(filename);
            using (var client = new Limilabs.FTP.Client.Ftp())
            {
                client.Connect(ConfigurationManager.AppSettings["FTP_HOST"]);
                client.Login(ConfigurationManager.AppSettings["FTP_USERNAME"], ConfigurationManager.AppSettings["FTP_PASSWORD"]);
                client.DeleteFile(fname);
                client.ChangeFolder(ConfigurationManager.AppSettings["FTP_ARCHIVE_FOLDER"]);
                client.Upload(fname, bytes);
            }
        }

        public static void ProcessFile(string filename)
        {
            logger.InfoFormat("Processing: {0}", filename);

            DeleteAllRecords(filename);

            var engine = new FileHelpers.MultiRecordEngine(EFSRecordTypeSelector, recordTypes);

            // To Read Use:
            var result = engine.ReadFile(filename);

            foreach (IEFSRecord item in result)
            {
                if (!String.IsNullOrEmpty(item.GetFiller()))
                {
                    logger.InfoFormat("Filler is not empty: {0}:{1}", item.GetType().Name, item.GetFiller());
                }
            }

            WriteToDB(result, filename);
            logger.Info("All Done");
        }

        public static void DeleteAllRecords(string filename)
        {
            var fname = Path.GetFileName(filename);
            logger.InfoFormat("Removing data for {0}", fname);
            var sql = @"
DELETE FROM TransactionSummary WHERE FileName=@0;
DELETE FROM TransactionLocation WHERE FileName=@0;
DELETE FROM TransactionItem WHERE FileName=@0;
DELETE FROM TransactionLineTax WHERE FileName=@0;
DELETE FROM TransactionLine WHERE FileName=@0;
";
            using (var db = new PetaPoco.Database("ShipExDB"))
            {
                db.Execute(sql, fname);
            }
        }

        public static void WriteToDB(object[] items, string filename)
        {
            logger.Info("Writing To DB");
            for (int i = 0; i < recordTypes.Length; i++)
            {
                var sourceType = recordTypes[i];
                var destType = outputTypes[i];
                var summaryItems = items.Where(s => s.GetType() == sourceType).Select(s => CopyObject(s, destType));
                /*
                var engine = new DelimitedFileEngine(destType);
                engine.HeaderText = GetHeaderText(destType);
                engine.WriteFile(destType.Name + ".csv", summaryItems);
                */
                var dbType = dbTypes[i];
                if (dbType != null)
                {
                    foreach (var item in summaryItems)
                    {
                        logger.DebugFormat("uploading record: {0}", item.GetType().Name);
                        var dbobj = CopyObject(item, dbType);
                        var saving = dbobj as ShipExDB.ISavable;
                        saving.CreatedDate = DateTime.Now;
                        saving.FileName = Path.GetFileName(filename);
                        //if (saving is ShipExDB.TransactionLocation)
                        //{
                        //    var tl = saving as ShipExDB.TransactionLocation;
                        //    if (ShipExDB.TransactionLocation.SingleOrDefault("SELECT * FROM TransactionLocation WHERE LocationId=@0", tl.LocationId) == null)
                        //    {
                        //        saving.Save();
                        //    }
                        //}
                        //else
                        //{
                            saving.Save();
                        //}
                    }
                }
            }
        }

        public static object CopyObject(object source, Type dest)
        {
            var constructor = dest.GetConstructor(new System.Type[] { });
            var result = constructor.Invoke(null);
            var resultType = dest;

            var sourceType = source.GetType();
            foreach (var field in sourceType.GetFields())
            {
                object sourceValue = field.GetValue(source);
                var destField = resultType.GetField(field.Name);
                if (destField != null)
                {
                    destField.SetValue(result, sourceValue);
                }
                else
                {
                    try
                    {
                        var destProp = resultType.GetProperty(field.Name);
                        if (destProp.PropertyType == typeof(decimal))
                        {
                            sourceValue = Convert.ToDecimal(sourceValue);
                        }
                        destProp.SetValue(result, sourceValue);
                    }
                    catch(Exception)
                    {
                        throw;
                    }
                }
            }

            return result;
        }

        public static string GetHeaderText(Type inputType)
        {
            var result = inputType.GetFields().Select(i => i.Name);
            return String.Join(",", result);
        }
    }
}
