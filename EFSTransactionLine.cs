﻿using System;
using FileHelpers;

namespace EFSTransactionParser
{
    [FixedLengthRecord]
    public class EFSTransactionLine: IEFSRecord
    {
        [FieldFixedLength(2)]
        [FieldTrim(TrimMode.Both)]
        public string RecordType;

        [FieldFixedLength(2)]
        [FieldTrim(TrimMode.Both)]
        public string Version;

        [FieldFixedLength(10)]
        [FieldTrim(TrimMode.Both)]
        public string SequenceNumber;

        [FieldFixedLength(10)]
        [FieldAlign(AlignMode.Right, '0')]
        public int TransactionId;

        [FieldFixedLength(2)]
        [FieldAlign(AlignMode.Right, '0')]
        public int LineId;

        [FieldFixedLength(4)]
        [FieldTrim(TrimMode.Both)]
        public string ProductCategory;

        [FieldFixedLength(7)]
        [FieldAlign(AlignMode.Right, '0')]
        [FieldConverter(typeof(DecimalConverter))]
        public double Quantity;

        [FieldFixedLength(11)]
        [FieldAlign(AlignMode.Right, '0')]
        [FieldConverter(typeof(DecimalConverter))]
        public double AmountDiscounted;

        [FieldFixedLength(11)]
        [FieldAlign(AlignMode.Right, '0')]
        [FieldConverter(typeof(DecimalConverter))]
        public double RetailAmount;

        [FieldFixedLength(8)]
        [FieldAlign(AlignMode.Right, '0')]
        public int FuelType;

        [FieldFixedLength(2)]
        [FieldTrim(TrimMode.Both)]
        public string ServiceType;

        [FieldFixedLength(2)]
        [FieldTrim(TrimMode.Both)]
        public string FuelUseType;

        [FieldFixedLength(7)]
        [FieldAlign(AlignMode.Right, '0')]
        [FieldConverter(typeof(DecimalConverter), 3)]
        public double PPUDiscounted;

        [FieldFixedLength(7)]
        [FieldAlign(AlignMode.Right, '0')]
        [FieldConverter(typeof(DecimalConverter), 3)]
        public double RetailPPU;

        [FieldFixedLength(3)]
        [FieldTrim(TrimMode.Both)]
        public string ProductNumber;

        [FieldFixedLength(1)]
        public string UnitOfMeasure;

        [FieldFixedLength(11)]
        [FieldAlign(AlignMode.Right, '0')]
        [FieldConverter(typeof(DecimalConverter))]
        public double DiscountAmount;

        [FieldFixedLength(200)]
        [FieldTrim(TrimMode.Both)]
        public string Filler;

        public override string GetFiller()
        {
            return Filler;
        }

        public static string GetRecordType()
        {
            return "TL";
        }
    }

    [DelimitedRecord(",")]
    public class TransactionLine
    {
        public string RecordType;
        public string Version;
        public string SequenceNumber;
        public int TransactionId;
        public int LineId;
        public string ProductCategory;
        public double Quantity;
        public double AmountDiscounted;
        public double RetailAmount;
        public int FuelType;
        public string ServiceType;
        public string FuelUseType;
        public double PPUDiscounted;
        public double RetailPPU;
        public string ProductNumber;
        public string UnitOfMeasure;
        public double DiscountAmount;
        public string Filler;
        public string FileName;
    }
}
