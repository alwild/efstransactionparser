﻿using System;
using FileHelpers;

namespace EFSTransactionParser
{
    [FixedLengthRecord]
    public class EFSFileHeader: IEFSRecord
    {
        [FieldFixedLength(2)]
        [FieldTrim(TrimMode.Both)]
        public string RecordType;

        [FieldFixedLength(2)]
        [FieldTrim(TrimMode.Both)]
        public string Version;

        [FieldFixedLength(10)]
        [FieldTrim(TrimMode.Both)]
        public string SequenceNumber;

        [FieldFixedLength(14)]
        [FieldConverter(ConverterKind.Date, "yyyyMMddHHmmss")]
        public DateTime FileDate;

        [FieldFixedLength(4)]
        [FieldTrim(TrimMode.Both)]
        public string FileType;

        [FieldFixedLength(4)]
        [FieldTrim(TrimMode.Both)]
        public string FileUse;

        [FieldFixedLength(264)]
        [FieldTrim(TrimMode.Both)]
        public string Filler;

        public static string GetRecordType()
        {
            return "HH";
        }

        public override string GetFiller()
        {
            return Filler;
        }
    }

    [DelimitedRecord(",")]
    public class FileHeader
    {
        public string RecordType;
        public string Version;
        public string SequenceNumber;
        public DateTime FileDate;
        public string FileType;
        public string FileUse;
        public string Filler;
        public string FileName;
    }
}
