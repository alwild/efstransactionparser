﻿using System;
using FileHelpers;

namespace EFSTransactionParser
{
    [FixedLengthRecord]
    public class EFSTransactionTrailer: IEFSRecord
    {
        [FieldFixedLength(2)]
        [FieldTrim(TrimMode.Both)]
        public string RecordType;

        [FieldFixedLength(2)]
        [FieldTrim(TrimMode.Both)]
        public string Version;

        [FieldFixedLength(10)]
        [FieldTrim(TrimMode.Both)]
        public string Sequence;

        [FieldFixedLength(10)]
        [FieldAlign(AlignMode.Right, '0')]
        public int TotalInfoRecords;

        [FieldFixedLength(10)]
        [FieldAlign(AlignMode.Right, '0')]
        public int TotalLineRecords;

        [FieldFixedLength(1)]
        public string TotalTransactionSign;

        [FieldFixedLength(11)]
        [FieldAlign(AlignMode.Right, '0')]
        [FieldConverter(typeof(DecimalConverter))]
        public double TotalTransactionAmount;

        [FieldFixedLength(254)]
        [FieldTrim(TrimMode.Both)]
        public string Filler;

        public override string GetFiller()
        {
            return Filler;
        }

        public static string GetRecordType()
        {
            return "TT";
        }
    }

    [DelimitedRecord(",")]
    public class TransactionTrailer
    {
        public string RecordType;
        public string Version;
        public string Sequence;
        public int TotalInfoRecords;
        public int TotalLineRecords;
        public string TotalTransactionSign;
        public double TotalTransactionAmount;
        public string Filler;
        public string FileName;
    }
}
