﻿using System;
using FileHelpers;

namespace EFSTransactionParser
{
    [FixedLengthRecord]
    public class EFSTransactionMisc: IEFSRecord
    {
        [FieldFixedLength(2)]
        [FieldTrim(TrimMode.Both)]
        public string RecordType;

        [FieldFixedLength(2)]
        [FieldTrim(TrimMode.Both)]
        public string Version;

        [FieldFixedLength(10)]
        [FieldTrim(TrimMode.Both)]
        public string Sequence;

        [FieldFixedLength(10)]
        [FieldAlign(AlignMode.Right, '0')]
        public int TransactionId;

        [FieldFixedLength(30)]
        [FieldTrim(TrimMode.Both)]
        public string Carrier;

        [FieldFixedLength(246)]
        [FieldTrim(TrimMode.Both)]
        public string Filler;

        public override string GetFiller()
        {
            return Filler;
        }

        public static string GetRecordType()
        {
            return "MI";
        }
    }

    [DelimitedRecord(",")]
    public class TransactionMisc
    {
        public string RecordType;
        public string Version;
        public string Sequence;
        public int TransactionId;
        public string Carrier;
        public string Filler;
        public string FileName;
    }
}
