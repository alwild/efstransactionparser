﻿using System;
using FileHelpers;

namespace EFSTransactionParser
{
    public class DecimalConverter : ConverterBase
    {
        private int decimal_places = 2;
        public DecimalConverter()
        { }

        public DecimalConverter(int places)
        {
            decimal_places = places;
        }

        public override object StringToField(string from)
        {
            int money = Convert.ToInt32(from);
            return (double)money / (Math.Pow(10, decimal_places));
        }

        public override string FieldToString(object from)
        {
            double money = Convert.ToDouble(from);
            return Math.Floor(money * (Math.Pow(10,decimal_places))).ToString();
        }
    }
}
